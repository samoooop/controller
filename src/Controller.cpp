#include <ros/ros.h>
#include <ros/time.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Point.h>
#include <geometry_msgs/Quaternion.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Int16.h>
#include <stdlib.h>
#include <nav_msgs/Odometry.h>
#include "PID.cpp"
#include <iostream>
#include <vector>
#include <cmath>
#include <string>
#include <tf/transform_datatypes.h>
#include <tf/transform_listener.h>
#include <geometry_msgs/Vector3.h>
#include <dynamic_reconfigure/server.h>
#include <controller/PIDConstantConfig.h>
#include <queue>
#include "PID_constant_helper.h"

#define boolToStr(a) a?"true":"false"
#define abs(a) a<0?-a:a
#define	MIN_ERROR 5.0e-2
#define epsilon 1.0e-7
#define MAX_OUT 2
#define MIN_OUT -1
char axis[6][5] = {"x","y","z","r","p","y"};
double cmdVelK[][3] = {{0.7,0.05,0},
					{0.7,0.05,0},
					{0.7,0,0},
					{0.15,0,0.1},
					{0.15,0,0.1},
					{0.1,0,0.1},
					}; //KP,KI,KD KP only cause order of acceleration
double fixPointK[][3] = {{0.7,0.1,0.2},
					{0.7,0.1,0.2},
					{3,0.1,0.2},
					{0.4,0.1,0.08},
					{0.3,0.05,0.2},
					{0.4,0.01,0.01},
					}; // KP,KI,KD
geometry_msgs::Pose fixPosition;
nav_msgs::Odometry previousState;
nav_msgs::Odometry currentState;
double cmd_vel[6]={0,0,0,0,0,0},position[7],vel[6];
double prevPosition[6],prevVel[6];
bool isStateArrived = false;
bool isFixed[] = {false,false,true,true,true,true};
bool canFixed[] = {true,true,true,true,true,true};
bool nearZeroBeforeFix[] = {false,false,false,false,false,false};
// double fixedPosition[7] = {0,0,-1,0,0,0,1}; // x y z ? ? ? set for default fix position;
double errorPosition[6] = {0,0,0,0,0,0};
double errorVelocity[6] = {0,0,0,0,0,0};
double out[6];
SPID *pidV,*pidP;
std::queue<geometry_msgs::Quaternion> fixPositionQueue;

int normalMode = 1; // fix z roll pitch
int freeMode = 2; // free roll pitch control
int barrelRollMode = 3; // for doing barrel roll fix depth yaw
int controllerMode = normalMode;
void validateValue(double&);
void PIDConstantCallBack(controller::PIDConstantConfig &config,uint32_t level);
void stateListenerCallBack(const nav_msgs::Odometry msg);
void cmd_velCallBack(const geometry_msgs::Twist msg);
void fixDepthCallBack(const std_msgs::Float64 msg);
void fixYawCallBack(const std_msgs::Float64 msg);
void changeFixedState();
void setPreviousState();
bool equal(double a, double b);
bool isClose(double a, double b);
bool outOfBound(double);
void calculateError();
void setK();
double angleError(double a,double b);
geometry_msgs::Twist calculatePID();
void cmd_fix_positionCallBack(const geometry_msgs::Point msg);
void cmd_fix_orientationCallBack(const geometry_msgs::Quaternion msg);
void modeCallback(const std_msgs::Int16 msg);
void fixAbsDepthCallBack(const std_msgs::Float64 msg);
void fixAbsYawCallBack(const std_msgs::Float64 msg);
void fixRelYawCallBack(const std_msgs::Float64 msg);
void pushSlerp(tf::Quaternion fq);

void init(){
	pidV = (SPID*)calloc(6,sizeof(SPID));
	pidP = (SPID*)calloc(6,sizeof(SPID));
	for(int i=0;i<6;i++){
		vel[i]=0;
		cmd_vel[i]=0;
		position[i]=0;
		pidV[i].resetPID();
		pidP[i].resetPID();
		pidP[i].setK(fixPointK[i][0],fixPointK[i][1],fixPointK[i][2]);
		pidV[i].setK(cmdVelK[i][0],cmdVelK[i][1],cmdVelK[i][2]);

	}
	fixPosition.orientation.x = 0;
	fixPosition.orientation.y = 0;
	fixPosition.orientation.z = 0;
	fixPosition.orientation.w = 1;
	PID_constant_helper::load_file("Controller"); // TODO if ros change this node name ?
}

int main(int argc,char **argv) {
	ros::init(argc,argv, "Controller");
	ros::NodeHandle nh;
	ros::Subscriber sub_state = nh.subscribe("/auv/state", 1000, &stateListenerCallBack);
	ros::Subscriber sub_cmd_vel = nh.subscribe("cmd_vel", 1000, &cmd_velCallBack);
	ros::Subscriber sub_cmd_fix_pos = nh.subscribe("cmd_fix_position", 1000, &cmd_fix_positionCallBack);
	ros::Subscriber sub_cmd_fix_orientation = nh.subscribe("cmd_fix_orientation", 1000, &cmd_fix_orientationCallBack);
	ros::Subscriber sub_controllerMode =  nh.subscribe("/controller/mode",100,&modeCallback);
	ros::Subscriber sub_fixAbsDepth = nh.subscribe("/fix/abs/depth", 1000, &fixAbsDepthCallBack);
	ros::Subscriber sub_fixRelYaw = nh.subscribe("/fix/rel/yaw", 1000, &fixRelYawCallBack);
	ros::Subscriber sub_fixAbsYaw = nh.subscribe("/fix/abs/yaw", 1000, &fixAbsYawCallBack);
	ros::Publisher pub = nh.advertise<geometry_msgs::Twist>("zeabus/cmd_vel",1000);
	ros::Publisher fixedPositionPublisher = nh.advertise<geometry_msgs::Pose>("controller/fixed_position",10);
	dynamic_reconfigure::Server<controller::PIDConstantConfig> server;
  	dynamic_reconfigure::Server<controller::PIDConstantConfig>::CallbackType f;
	init();
  	f = boost::bind(&PIDConstantCallBack, _1, _2);
  	server.setCallback(f);
	ros::Rate rate(100);
	while(nh.ok()) {
		ros::spinOnce();
		if(!isStateArrived){
			ROS_INFO("No state arrived! wait 1 sec");
			ros::Duration(1).sleep();
			continue;
		}
		changeFixedState();
		if(!fixPositionQueue.empty()){
			fixPosition.orientation = fixPositionQueue.front();
			fixPositionQueue.pop();
			//printf("%lf %lf %lf\n",fixPosition.orientation.x,fixPosition.orientation.y,fixPosition.orientation.z);
		}
		rate.sleep();
		pub.publish(calculatePID());
		fixedPositionPublisher.publish(fixPosition);
		//  printf("Vel      %.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\n",vel[0],vel[1],vel[2],vel[3],vel[4],vel[5]);
		// printf("cmd Vel  %.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\n",cmd_vel[0],cmd_vel[1],cmd_vel[2],cmd_vel[3],cmd_vel[4],cmd_vel[5]);
		//  printf("errorV   %.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\n",errorVelocity[0],errorVelocity[1],errorVelocity[2],errorVelocity[3],errorVelocity[4],errorVelocity[5]);
		// printf("errorP   %.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\n",errorPosition[0],errorPosition[1],errorPosition[2],errorPosition[3],errorPosition[4],errorPosition[5]);
		// printf("isFixed    %s\t%s\t%s\t%s\t%s\t%s\n",boolToStr(isFixed[0]),boolToStr(isFixed[1]),boolToStr(isFixed[2]),boolToStr(isFixed[3]),boolToStr(isFixed[4]),boolToStr(isFixed[5]));
		// printf("Out      %.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\n",out[0],out[1],out[2],out[3],out[4],out[5]);
		// printf("Pos      %.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\n",position[0],position[1],position[2],position[3],position[4],position[5],position[6]);
		// printf("FixPos   %.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\t%.2lf\n",fixPosition.position.x
		//  																	,fixPosition.position.y
		//  																	,fixPosition.position.z
		//  																	,fixPosition.orientation.x
		//  																	,fixPosition.orientation.y
		//  																	,fixPosition.orientation.z
		//  																	,fixPosition.orientation.w);
		// printf("\n\n");
		setK();
	}
}

void stateListenerCallBack(const nav_msgs::Odometry msg){

	currentState = msg;
	if(isStateArrived == false){
		tf::Quaternion ini(msg.pose.pose.orientation.x,msg.pose.pose.orientation.y,msg.pose.pose.orientation.z,msg.pose.pose.orientation.w);
		tfScalar R,P,Y;
		tf::Matrix3x3(ini).getRPY(R,P,Y);
		tf::Quaternion fixY;
		fixY.setRPY(0,0,Y);
		tf::quaternionTFToMsg(fixY,fixPosition.orientation);
	}
	isStateArrived = true;
	position[0] = msg.pose.pose.position.x;
	position[1] = msg.pose.pose.position.y;
	position[2] = msg.pose.pose.position.z;
	position[3] = msg.pose.pose.orientation.x;
	position[4] = msg.pose.pose.orientation.y;
	position[5] = msg.pose.pose.orientation.z;
	position[6] = msg.pose.pose.orientation.w;
	vel[0] = msg.twist.twist.linear.x;
	vel[1] = msg.twist.twist.linear.y;
	vel[2] = msg.twist.twist.linear.z;
	vel[3] = msg.twist.twist.angular.x;
	vel[4] = msg.twist.twist.angular.y;
	vel[5] = msg.twist.twist.angular.z;
}

void cmd_velCallBack(const geometry_msgs::Twist msg){
	double cmd_vel_in[6];
	cmd_vel_in[0] = msg.linear.x;
	cmd_vel_in[1] = msg.linear.y;
	cmd_vel_in[2] = msg.linear.z;
	cmd_vel_in[3] = msg.angular.x;
	cmd_vel_in[4] = msg.angular.y;
	cmd_vel_in[5] = msg.angular.z;
	for(int i = 0;i < 6;i++){
		if(!isClose(cmd_vel_in[i],cmd_vel[i])){
			pidV[i].resetPID();
		}
		cmd_vel[i] = cmd_vel_in[i];
	}
}

void cmd_fix_positionCallBack(const geometry_msgs::Point msg){
	isFixed[0] = true;
	isFixed[1] = true;
	isFixed[2] = true;
	fixPosition.position = msg;
}

void cmd_fix_orientationCallBack(const geometry_msgs::Quaternion msg){
	isFixed[3] = true;
	isFixed[4] = true;
	isFixed[5] = true;
	fixPosition.orientation = msg;
}

void setPreviousState(){
	for(int i=0;i<6;i++){
		prevPosition[i] = position[i];
		prevVel[i] = position[i];
	}
}
template<typename T>
void copy(T &a,T &b,size_t n){
	while(n--)a[n]=b[n];
}
void changeFixedState(){
	double fixedPosition[7] = {fixPosition.position.x,
							fixPosition.position.y,
							fixPosition.position.z,
							fixPosition.orientation.x,
							fixPosition.orientation.y,
							fixPosition.orientation.z,
							fixPosition.orientation.w};
	// Position State
	for(int i = 0;i < 6;i++){
		// !fix -> fix
		if(equal(0.0,cmd_vel[i]) && !isFixed[i] && (isClose(vel[i],0.0) || !nearZeroBeforeFix[i]) && canFixed[i]){
			isFixed[i] = true;
			fixedPosition[i] = position[i];
			pidP[i].resetPD();
		}
		// fix -> !fix
		else if(!equal(0.0,cmd_vel[i]) && isFixed[i]){
			isFixed[i] = false;
			pidV[i].resetPD();
		}
		if(!isFixed[i]){
			fixedPosition[i] = position[i];
		}
	}

	if(controllerMode == freeMode){
		if(!isFixed[0] || !isFixed[1] || !isFixed[2]){
			isFixed[0] = false;
			isFixed[1] = false;
			isFixed[2] = false;
			//copy(fixedPosition,position,3);
			fixedPosition[0] = position[0];
			fixedPosition[1] = position[1];
			fixedPosition[2] = position[2];
		}
		if(!isFixed[3] || !isFixed[4] || !isFixed[5]){
				isFixed[3] = false;
				isFixed[4] = false;
				isFixed[5] = false;
				fixPosition = currentState.pose.pose;
		}
	}
	if(controllerMode == barrelRollMode){
		if(!isFixed[0] || !isFixed[1] || !isFixed[2]){
			isFixed[0] = false;
			isFixed[1] = false;
			//isFixed[2] = false;
			//copy(fixedPosition,position,3);
			fixedPosition[0] = position[0];
			fixedPosition[1] = position[1];
			//fixedPosition[2] = position[2];
		}
		if(!isFixed[3] || !isFixed[4] || !isFixed[5]){
				isFixed[3] = false;
				//isFixed[4] = false;
				//isFixed[5] = false;
				fixPosition.orientation = currentState.pose.pose.orientation;
		}
	}else if(controllerMode == normalMode){
		if(!isFixed[0] || !isFixed[1]){
			isFixed[0] = false;
			isFixed[1] = false;
			fixedPosition[0] = position[0];
			fixedPosition[1] = position[1];
		}
	}
	if(!isFixed[3] || !isFixed[4] || !isFixed[5]){
		fixedPosition[6] = position[6];
	}
	fixPosition.position.x = fixedPosition[0];
	fixPosition.position.y = fixedPosition[1];
	fixPosition.position.z = fixedPosition[2];
	fixPosition.orientation.x = fixedPosition[3];
	fixPosition.orientation.y = fixedPosition[4];
	fixPosition.orientation.z = fixedPosition[5];
	fixPosition.orientation.w = fixedPosition[6];
}

void calculateError(){

	// Calculate Orientation error

	tf::Quaternion fq(fixPosition.orientation.x,
                    fixPosition.orientation.y,
                    fixPosition.orientation.z,
                    fixPosition.orientation.w);

  	tf::Quaternion pq(currentState.pose.pose.orientation.x,
                    currentState.pose.pose.orientation.y,
                    currentState.pose.pose.orientation.z,
                    currentState.pose.pose.orientation.w);

	tf::Quaternion p =  fq*pq.inverse();

	//tf::Quaternion nq(p.getAxis().getX()
	//				,p.getAxis().getY()
	//				,p.getAxis().getZ()
	//				,0.0);
	//double L = nq.getAngle();
	//nq.normalize();

	pq = pq.inverse() * p * pq;
	geometry_msgs::Quaternion neww;
	tf::quaternionTFToMsg(pq,neww);
	tfScalar R,P,Y;
	tf::Matrix3x3(pq).getRPY(R,P,Y);
	errorPosition[3] = R;
	errorPosition[4] = P;
	errorPosition[5] = Y;

	//printf("RPY %lf %lf %lf\n",R,P,Y);

	// END OF CALCUALTION


	// Calcualate position error

	tf::Quaternion Pin(fixPosition.position.x - currentState.pose.pose.position.x
						,fixPosition.position.y - currentState.pose.pose.position.y
						,fixPosition.position.z - currentState.pose.pose.position.z
						,0);
	double Len = Pin.length();
	if(!equal(Len,0)){
	Pin.normalize();
	tf::Quaternion Q(-currentState.pose.pose.orientation.x
					,-currentState.pose.pose.orientation.y
					,-currentState.pose.pose.orientation.z
					,currentState.pose.pose.orientation.w);
	Q = Q * Pin * Q.inverse();
	geometry_msgs::Quaternion errorP;
	tf::quaternionTFToMsg(Q,errorP);
	errorPosition[0] = errorP.x*Len;
	errorPosition[1] = errorP.y*Len;
	errorPosition[2] = errorP.z*Len;
	}
	else{

	errorPosition[0] = 0;
	errorPosition[1] = 0;
	errorPosition[2] = 0;
	}
	//END of calculation

	for(int i = 0;i < 6;i++){
		if(i>=3){
			if(fabs(errorPosition[i])>M_PI){
				if(errorPosition[i]>0)
					errorPosition[i]-=2*M_PI;
				else
					errorPosition[i]+=2*M_PI;
			}
		}
		errorVelocity[i] = cmd_vel[i] - vel[i];
	}
}

geometry_msgs::Twist calculatePID(){
	geometry_msgs::Twist t;
	calculateError();
	//double out[6];
	double max = 0;
	for(int i = 0;i<6;i++){
		out[i] = pidV[i].pid(errorVelocity[i]);
		if(isFixed[i]){
			out[i] = pidP[i].pid(errorPosition[i]);
		}
		if(i == 0 || (fabs(out[i]) > max)){
			max = fabs(out[i]);
		}
		if(fabs(out[i])>1)
			out[i]/=fabs(out[i]);
		//validateValue(out[i]);
	}
	//to limit the output in the same ratio
	// if(max > 1){

	// 	for(int i=0 ;i<6;i++){
	// 		out[i]/=max;
	// 	}
	// }
	// dont allow Controller to stabilize both rotation and position
	// if((fabs(errorPosition[3])+fabs(errorPosition[4])+fabs(errorPosition[5])) > 0.1) {
	// 	printf("!!!TOO MUCH rotation error!!! %.2lf\n",abs(errorPosition[3])+abs(errorPosition[4])+abs(errorPosition[5]));
	// 	out[0] = 0;
	// 	out[1] = 0;
	// 	out[2] = 0;
	// }
	t.linear.x = out[0];
	t.linear.y = out[1];
	t.linear.z = out[2];
	t.angular.x = out[3];
	t.angular.y = out[4];
	t.angular.z = out[5];
	return t;
}
bool outOfBound(double x){
	return x < MIN_OUT || x > MAX_OUT || x!=x;
}
void validateValue(double& x){
	if(x != x){
		printf("%lf ->",x);
		x = 0;
		printf("%lf\n", x);
	}
	else if(x > MAX_OUT){
		printf("%lf ->", x);
		x = MAX_OUT;
		printf("%lf\n", x);
	}
	else if(x < MIN_OUT){
		printf("%lf ->", x);
		x = MIN_OUT;
		printf("%lf\n", x);
	}
}
double Ang(double a){
	if(a>0)
		return -M_PI-(M_PI-a);
	return M_PI+(a+M_PI);
}
bool equal(double a, double b){ // for double error
    return fabs(a - b) < epsilon;
}

bool isClose(double a,double b){ // for sensor error
	return fabs(a - b) < MIN_ERROR;
}

void PIDConstantCallBack(controller::PIDConstantConfig &config,uint32_t level){
	ROS_INFO("!!!--K changed--!!!");

	cmdVelK[0][0] = config.KPVx;
	cmdVelK[0][1] = config.KIVx;
	cmdVelK[0][2] = config.KDVx;

	cmdVelK[1][0] = config.KPVy;
	cmdVelK[1][1] = config.KIVy;
	cmdVelK[1][2] = config.KDVy;

	cmdVelK[2][0] = config.KPVz;
	cmdVelK[2][1] = config.KIVz;
	cmdVelK[2][2] = config.KDVz;

	cmdVelK[3][0] = config.KPVroll;
	cmdVelK[3][1] = config.KIVroll;
	cmdVelK[3][2] = config.KDVroll;

	cmdVelK[4][0] = config.KPVpitch;
	cmdVelK[4][1] = config.KIVpitch;
	cmdVelK[4][2] = config.KDVpitch;

	cmdVelK[5][0] = config.KPVyaw;
	cmdVelK[5][1] = config.KIVyaw;
	cmdVelK[5][2] = config.KDVyaw;

	fixPointK[0][0] = config.KPPx;
	fixPointK[0][1] = config.KIPx;
	fixPointK[0][2] = config.KDPx;

	fixPointK[1][0] = config.KPPy;
	fixPointK[1][1] = config.KIPy;
	fixPointK[1][2] = config.KDPy;

	fixPointK[2][0] = config.KPPz;
	fixPointK[2][1] = config.KIPz;
	fixPointK[2][2] = config.KDPz;

	fixPointK[3][0] = config.KPProll;
	fixPointK[3][1] = config.KIProll;
	fixPointK[3][2] = config.KDProll;

	fixPointK[4][0] = config.KPPpitch;
	fixPointK[4][1] = config.KIPpitch;
	fixPointK[4][2] = config.KDPpitch;

	fixPointK[5][0] = config.KPPyaw;
	fixPointK[5][1] = config.KIPyaw;
	fixPointK[5][2] = config.KDPyaw;

	PID_constant_helper::dump_file(cmdVelK,fixPointK);
}
void fixAbsDepthCallBack(const std_msgs::Float64 msg){
	isFixed[2] = true;
	pidP[2].resetPD();
	fixPosition.position.z = msg.data;
}

void fixAbsYawCallBack(const std_msgs::Float64 msg){
	isFixed[5] = true;
	pidP[5].resetPD();

	// only work on Normal Mode
	// current quaternion -> msg (Absolute)
	//fixPosition.orientation = tf::createQuaternionMsgFromYaw(msg.data);
	tf::Quaternion fq;
	fq.setRPY(0,0,msg.data);
	pushSlerp(fq);
}

void pushSlerp(tf::Quaternion fq){
	tf::Quaternion pq(currentState.pose.pose.orientation.x
					,currentState.pose.pose.orientation.y
					,currentState.pose.pose.orientation.z
					,currentState.pose.pose.orientation.w);
	tf::Quaternion dq = fq * pq.inverse();
	double interval = fabs(dq.getAngleShortestPath());
	interval = interval / M_PI * 1000.0;
	for(double i=1;i<=interval;i++){
		geometry_msgs::Quaternion q;
		quaternionTFToMsg(pq.slerp(fq,i/interval),q);
		fixPositionQueue.push(q);
	}
}

void fixRelYawCallBack(const std_msgs::Float64 msg){
	isFixed[5] = true;
	pidP[5].resetPD();

	// To rotate current quaternion -> current quaternion + msg (Relative)
	tf::Quaternion currentOrientation(fixPosition.orientation.x
					,fixPosition.orientation.y
					,fixPosition.orientation.z
					,fixPosition.orientation.w);
	tf::Quaternion rotationQuaternion;
	rotationQuaternion.setRPY(0,0,msg.data);
	pushSlerp(currentOrientation*rotationQuaternion);
	// tf::quaternionTFToMsg(currentOrientation*rotationQuaternion,fixPosition.orientation);
}

void setK(){
	for(int i=0;i<6;i++){
		pidP[i].setK(fixPointK[i][0],fixPointK[i][1],fixPointK[i][2]);
		pidV[i].setK(cmdVelK[i][0],cmdVelK[i][1],cmdVelK[i][2]);
	}
}
void modeCallback(const std_msgs::Int16 msg){
	if(msg.data == 1){
		controllerMode = 1;
		//TODO set rotation
	}
	else if(msg.data == 2){
		controllerMode = 2;
	}
	else if(msg.data == 3){
		controllerMode = 3;
	}
	else{
		ROS_WARN("Controller got wrong mode code");
	}
}
